%% Rank Testing

for i =1:100
   x_size = randi(100);
   y_size = randi(100);
   A = rand(x_size,y_size);
   original_rank = rank(A);
   std_A = zscore(zscore(A')');
   if(rank(std_A)==(original_rank-1))
       disp('true');
   else
       disp('false');
   end
end
