function assert_SVD_equals_EIG(Z, condition)
[m, n] = size(Z);
[U,D,V]=svd(Z, 'econ');
lambda = eig(Z'*Z/(m-1));
try 
    assert(abs(sum(lambda)-trace(D.^2))<0.001);
catch
    warning(['SVD fails on condition: ' condition]);
end
end