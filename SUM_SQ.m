%%
%%
% For H:
% use Z instead of Znorm
% comment the Between and Within Section
% uncomment the size of GC, E and the Perc_Zh in results
% uncomment the following 4 lines of code

% Z=Z';
% [m n] = size(Z);
% GC = ZH';
% E = E';


%%
% Calculating the total SUM of squares
% Subtract total mean from each value. Square it. Sum all values up. 

load ('Z_takane106-7.mat')
assert_SS_equals_SVD(Z, 'Raw Score'); %%Raw score
assert_SVD_equals_EIG(Z, 'Raw Score');
Z_center = bsxfun(@minus, Z, mean(Z));
assert_SS_equals_SVD(Z_center, 'Columns Centered');
assert_SVD_equals_EIG(Z, 'Columns Centered');
Z_center = bsxfun(@minus, Z', mean(Z'))';
assert_SS_equals_SVD(Z_center, 'Rows Centered');

Znorm = zscore(Z); %%column normalization
assert_SS_equals_SVD(Znorm, 'Columns Standardized');

Znorm = zscore(zscore(Z')'); %%row and column normalization
assert_SS_equals_SVD(Znorm, 'Columns and Rows Standardized');



%% 
% Calculating the Sum of Squares for GC

[mgc ngc] = size(GC); 

mean_GC = mean(mean(GC));
Sq_GC = (GC-mean_GC).^2;

SSq_GC=zeros(1,ngc); %make new variable for the sum of columns

% compute sum of squares for each column

for i = 1:ngc
    SSq_GC(1,i) = sum(Sq_GC(:,i));
end

SSq_GC = sum(SSq_GC);  

%% 
% Calculating the Sum of Squares Between 
% Between SSQs are calculated by subtracting the mean of each column by
% mean Z for each value. Square that and sum it. 

SSq_B=zeros(1,n);

mean_columns = mean(Z); % this gives us vector of n columns, containing the mean of each column

for i = 1:n
    SSq_B(1,i) = m*((mean_columns(1,i)-meanZ).^2); %mean of column n minus total mean, square this. and do this for each value (this is equivalent to multiplication of number of rows = m )
end

SSq_B = sum(SSq_B); % Sum the squares for each column

%% 
% Calculating the Sum of Squares for E

[me ne] = size(E); %uncomment when using H

mean_E = mean(mean(E));
Sq_E = (E-mean_E).^2;

SSq_E=zeros(1,ne); %make new variable for the sum of columns

% compute sum of squares for each column

for i = 1:ne
    SSq_E(i) = sum(Sq_E(:,i));
end

SSq_E = sum(SSq_E);


%% Calculating the Sum of Squares Within
% Calculate mean for each column. Each value of column n needs to be subtracted by mean of column n. 
% Square these values and sum all sqared values.

mean_columns = mean(Z); % this gives us vector of n columns, containing the mean of each column
   
Sq_W = zeros(m,n);      % Matrix containing zeros  

for num_col = 1:n;      % Substracts mean of column n from each value (m is the number of rows) in column n. 
    for num_rows = 1:m;
    Sq_W(num_rows,num_col) =Z(num_rows,num_col)-mean_columns(1,num_col); %Gives 
    end
end

Sq_W2 = Sq_W.^2;       % Square the values in Sq_W - this can be in loop as well; but if seperate easier to take record

SSq_W = sum(sum(Sq_W2));  %sum(Sq_W2) gives sum for each column, sum(sum(Sg_W2))sums all columns.


%%
% Results

Perc_GC = SSq_GC/SSq_Z; 
%Perc_ZH = SSq_GC/SSq_Z; 
Perc_E = SSq_E/SSq_Z;

% using diagonal of D
zsum    = sum(diag(D.^2));
gcsum   = sum(diag(D_GC.^2));
%zhsum   = sum(diag(D_ZH.^2));
esum    = sum(diag(D_E.^2));

PercD_GC = gcsum/zsum;
PercD_GC = gesum/zsum;

%% F-Test
% H0: No differences BETWEEN groups
% H1: Differences BETWEEN groups

% F-value is SSB/degrees of freedom B devided by SSW/degrees of Freedom
% within

F = (SSq_B/(m-1))/(SSq_W/(m*(n-1)));

% Look up critical Value for df1 m-1 and df2 m*(n-1) in F-table for specific alpha value.  
