%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%CPCA course Todd Woodward
%Example Data from Hunter & Takane Paper (2002) - Z_takane106-7.mat
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% We have a data set Z containing ratings of 11 clinicans for 4 different patients.
% On 17 Variables. The data is structured in a 44 by 17 matrix. We assume
% the following linear model: Z = model + error. Steps: PCA on Z, PCA on
% model, .. 


%Determining Eigenvalues

load ('Z_takane106-7.mat')

% Standarize Dataset

[m n]=size(Z);                  % n= number of columns, in our case 17; m = number of rows, in our case 44
Znorm1=zscore(Z')';
Znorm=zscore(Znorm1);
%Znorm = Z;

NrComp = 2;                     % Enter amount of components you like to look at 


EIG = eigs((corrcoef(Znorm)));  % function "eigs" calculates 6 largest Eigenvalues. 
                                % "eig" calculates all Eigenvalues, but they are not sorted. 
%rank(Z);                       % equals variance, in this case it is 17. 
                                % Because Z has 17 independent variables, which are set to have a variance equal to 1
                                
                                % svd(Z,econ) - this gives other results in my version.
                                % svd(Z,0) makes more sense
                            
                                
[U D V] = svd(Znorm, 0);        % U = component score = left singular vector 
                                %     gives the importance of the component for each data row, 
                                %     which in this case is each psychiatrist/patient combination.
                                % D = singular values
                                % V = component loadings/correlations; 
                                %     needs to be rescaled to get loadings; 
                                %     important for figure one V gives the importance of each variable to the
                                %     component. That's what is plotted in the figure.

%EIG2=sqrt((diag(D)/(m-1)));                                
EIG2=(diag(D).^2/(m-1));     %svd needs to be converted into Eigenvalues: squareroot of svd divided by N-1
EIG2s=sqrt(diag(D).^2/(m-1));

EIGz_correct=diag(D).^2;						

% Using values of the first two components

truncV = V(1:n,1:NrComp); 
truncU = U(1:m,1:NrComp);
truncD = D(1:NrComp,1:NrComp);
  
% calculate the component loadings for these two compents; m is defined
% earlier and referes to the total ratings (4 patients rated by 11
% psychiartists = 44)

comp1=V(:,1)*truncD(1,1)/sqrt(m-1);
comp2=V(:,2)*truncD(2,2)/sqrt(m-1);

%comploading = truncV*truncD/sqrt(m-1); %comp1 and comp2 in one variable
                                       % For all components
                                       % comploadings_all=(V*D)/sqrt(m-1)
                                       %comploading1=comploadings_all(1,:);
                                       %comploading2=comploadings_all(2,:);


%Plot Component loadings for each variable

symptomlabels = {'somatic concern','anxiety','emotional withdrawal','conceptual disorganization',...
    'guilt feelings','tension','mannerisms and posturing','grandiosity','depressive mood','hostility',...
    'suspicious','hallucinatory behavior','motor retardation','uncooperativeness','unusual thought content',...
    'blunted affect','excitement'};
fig1 = figure;
scatter(comp1, comp2);
xlabel =('comp1');
ylabel =('comp2');
text(comp1, comp2, symptomlabels);
title( 'Figure 1a - symptom loadings');

% Plot eigenvalues
fig2 = figure;
plot(EIG);
title( 'Eigenvalues of components in Z');

% Results: This gives us precentage of total explained variance 
VarComp = EIG/rank(Z);


%% PCA with G; z = model + error make PCA on our model
% in this case, we want to know how much of the variance can be explained
% by the difference in patients.

% Create G variable - This is a dummy variable containing information about
% our model. In this case we have 4 patients and 44 variable. G is a 44 by 4 matrix.
% Gzeros=zeros(44,4);. Change values manually. First 11 rows [1 0 0 0], row 11 until 22 [0 1 0 0], etc. 
                                                
load('G.mat');

C=inv(G'*G)*G'*Znorm;          %same thing: C=((G'*G)^-1)*G'*Znorm;

GC = G*C;                      % GC is y hat, this is the matrix of our betas 


% sum_GC = sum(var(GC));      
% sum_Z = sum(var(Znorm));
                                 % same as: sum_GC = sum(std(GC).^2);
                                 % Had this at the beginnig, but doesn�t make sense: sum_GC = sum(std(GC));
                                 % In order to get the sum squares as in
                                 % ANOVA; multiply sum_Z/sum_E/sum_GC by m (= number of rows)
                                 
                                 
% Var_Pat = sum_GC/sum_Z;        % Variance explained by differences in patients = within

EIG_GC = eigs((corrcoef(GC))); % notes: corrcoef ([U G]); or probably ([U_GC G]);
			
[U_GC D_GC V_GC] = svd(GC, 0);

EIGgc_correct=diag(D_GC).^2;

truncV_GC = V_GC(1:n,1:NrComp); 
truncU_GC = U_GC(1:m,1:NrComp);
truncD_GC = D_GC(1:NrComp,1:NrComp);

% compall_GC = truncV_GC*truncD_GC/sqrt(m-1);
% 
% comp1_GC = V_GC(:,1)*truncD_GC(1,1)/sqrt(m-1);
% comp2_GC = V_GC(:,2)*truncD_GC(2,2)/sqrt(m-1);

%EIG2_GC=sqrt((diag(D_GC)/(m-1)));
EIG2_GC=(diag(D_GC).^2/(m-1));
EIG2s_GC=sqrt(diag(D_GC).^2/(m-1));

% fig = figure;
% scatter(comp1_GC, comp2_GC);
% text(comp1_GC, comp2_GC, symptomlabels);
% title( 'symptom loadings patients');


Loadings_GC=corrcoef([truncU_GC GC]);
Fig1bloadings = Loadings_GC;

fig3=figure;
plot(Fig1bloadings((NrComp+1):end, 1), Fig1bloadings((NrComp+1):end, 2), '.');
xlabel =('Between1');
ylabel =('Between2');
text(Fig1bloadings((NrComp+1):end, 1), Fig1bloadings((NrComp+1):end, 2), symptomlabels);
title( 'Figure1b - Component loadings between analysis');



% describing the results
GC_singVec = diag(D_GC);
GC_EIGval=diag(D_GC^2./(m-1));
    Var_GC_total=sum(diag(D_GC^2./(m-1))); % Sums the eigenvalues of GC. 
    GC_Precent=(Var_GC_total/rank(Z)*100); 
    GC_comp_Var = GC_EIGval/(rank(Z))*100; %% how much each GC component contributes to total variance
    GC_comp_between = GC_EIGval/Var_GC_total*100; %% how much does each GC component contribute to the variance explainable by GC?

GC_EIGval2=diag(sqrt(D_GC^2./(m-1)));
    Var_GC_total2=sum(sqrt(diag(D_GC^2./(m-1)))); % Sums the eigenvalues of GC. 
    GC_Precent2=(Var_GC_total2/rank(Z)*100); 
    GC_comp_Var2 = GC_EIGval2/(rank(Z))*100; %% how much each GC component contributes to total variance
    GC_comp_between2 = GC_EIGval2/Var_GC_total2*100; %% how much does each GC component contribute to the variance explainable by GC?
    
    
    
% std(GC) 
% sum(std(GC))

%% E = this is what is "between", 

E = Znorm - GC; 

% std(E);
% sum_E = sum(std(E));
% sum_E = sum(var(E));
     

EIG_E = eigs((corrcoef(E))); 

[U_E D_E V_E] = svd(E, 0);

EIGe_correct=diag(D_E).^2;

truncV_E = V_E(1:n,1:NrComp); 
truncU_E = U_E(1:m,1:NrComp);
truncD_E = D_E(1:NrComp,1:NrComp);

% compall_E = truncV_E*truncD_E/sqrt(m-1);
% comp1_E = V_E(:,1)*truncD_E(1,1)/sqrt(m-1);
% comp2_E = V_E(:,2)*truncD_E(2,2)/sqrt(m-1);

%EIG2_E=sqrt((diag(D_E)/(m-1)));
EIG2_E=(diag(D_E).^2/(m-1));

% fig = figure;
% scatter(comp1_E, comp2_E);
% text(comp1_E, comp2_E, symptomlabels);
% title( 'symptom loadings clinician ratings');

Loadings_E=corrcoef([truncU_E E]);
Fig1cloadings = Loadings_GC;

fig4=figure;
plot(Fig1cloadings((NrComp+1):end, 1), Fig1cloadings((NrComp+1):end, 2), '.');
text(Fig1cloadings((NrComp+1):end, 1), Fig1cloadings((NrComp+1):end, 2), symptomlabels); 
xlabel =('Within1');
ylabel =('Within2');
title( 'Figure1c - Component loadings within');

E_singVec = diag(D_E);
E_EIGval=diag(D_E^2./(m-1));
    Var_E_total=sum(diag(D_E^2./(m-1))); % Sums the eigenvalues of E.
    E_Precent=(Var_E_total/rank(Z)*100); 
    E_comp_Var = E_EIGval/(rank(Z))*100; %% how much each E component contributes to total variance
    E_comp_between = E_EIGval/Var_E_total*100; %% how much does each E component contribute to the variance explainable by E?

E_singVec = diag(D_E);
E_EIGval2=diag(sqrt(D_E^2./(m-1)));
    Var_E_total2=sum(diag(sqrt(D_E^2./(m-1)))); % Sums the eigenvalues of E.
    E_Precent2=(Var_E_total2/rank(Z)*100); 
    E_comp_Var2 = E_EIGval2/(rank(Z))*100; %% how much each E component contributes to total variance
    E_comp_between2 = E_EIGval2/Var_E_total2*100; %% how much does each E component contribute to the variance explainable by E?



%% Figure 1 d:

Predictor_weights=corrcoef([truncU_GC G]);


Fig1dloadings=corrcoef([truncU_GC G]); % First 2 rows (or if more components # 
                                       % of compents rows) show
                                       % intercorrelations between the
                                       % predictors. All additional rows
                                       % showing predictor loadings. 

fig5=figure;
plot(Fig1dloadings((NrComp+1):end, 1), Fig1dloadings((NrComp+1):end, 2), '.');
labelsG = {'MDD','MDM','SimpSchizophrenia','ParaSchizophrenia'};
text(Fig1dloadings((NrComp+1):end, 1), Fig1dloadings((NrComp+1):end, 2), labelsG);
xlabel =('Predictor1');
ylabel =('Predictor2');
title( 'Figure 1d - Predictor loadings on contrained solution');
    
% %% Clinican Ratings
% 
% % Create a differnt model. In this case we would like to estimate
% % the variance, explained by differences in the Clinicians rating. 
% % There are 11 different clinicans. Create a 44 by 11 matrix. Pzeros=zeros(44,11);
% % Change values manually row 1 [1 0 0 0 0 0 0 0 0 0 0], row 2 [0 1 0 0 0 0
% % 0 0 0 0 0],...,  row 11 [0 0 1 0 0 0 0 0 0 0 1]. row 12 will be same as
% % row 1, etc. 
% 
% load('P.mat');
% 
% C2=inv(P'*P)*P'*Znorm;
% 
% PC = P*C2; 
% 
% EIG_PC = eigs((corrcoef(PC)));
%                              
% [U_P D_P V_P] = svd(PC, 0);
% 
% %EIG2_PC=sqrt((diag(D_P)/(m-1)));
% EIG2_PC=(diag(D_P).^2/(m-1));
% 
% 
% truncV_P = V_P(1:n,1:NrComp); 
% truncU_P = U_P(1:m,1:NrComp);
% truncD_P = D_P(1:NrComp,1:NrComp);
% 
% loadings_P=corrcoef([truncU_P P]);
% 
% fig6 = figure;
% clinicanlabels={'1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11'}
% plot(loadings_P((NrComp+1):end,1), loadings_P((NrComp+1):end,2), '.');
% text(loadings_P((NrComp+1):end,1), loadings_P((NrComp+1):end,2), clinicanlabels);
% 
% P_singVec = diag(D_P)
% P_EIGval=diag(D_P^2./(m-1));
%     Var_P_total=sum(diag(D_P^2./(m-1))) % Sums the eigenvalues of P.
%     P_Precent=(Var_P_total/rank(Z)*100) 
%     P_comp_Var = P_EIGval/(rank(Z))*100 %% how much each P component contributes to total variance
%     P_comp_between = P_EIGval/Var_P_total*100 %% how much does each P component contribute to the variance explainable by P?
%  
%   %This gives us a slighly different result as E (38% of Variance explained by E). The Variance explained by
%   %the clinican�s ratings is according to this analysis only 10%. This means
%   %there is probably an interaction between the within and between factor,
%   %but we do not have enough degrees of freedom to test the interaction
%   %effect. By applying PCA to E we are overestimating the influence of
%   %within factor.

    
%% Tabelle

%                                   Internal Analysis
% Source            External Analysis	1               2          
% Total                 rank(Z)        EIG(1,1)        EIG(2,1)        

 
% Between               Var_GC_total   GC_EIGval(1,1)  GC_EIGval(2,1)    

% Within                Var_E_total    E_EIGval(1,1)   E_EIGval(2,1)     

% For column 1+2 add values, percentages of total devide by rank(Z), percentages within/between devide by Var_GC/E_total  
% Sum of Squares can be calculated by multiplying the Eigenvalues by m (which is the number of rows of Z).  

%% COMPARE with Factoranalysis in SPSS

comp_scores_SPSS = zscore(U);       % matches component scores in SPSS.
comp_loadings_SPSS = corrcoef(U,Z); % matches component loadings in SPSS
V_SPSS = corrcoef(U,Z);             % matches our method of rescaling the right singular vector (divide by m-1 not m)
V_SPSS = V_SPSS/m-1;
corr_matrix=corr(Z);


%% Covariance

Predictor_weights_cov_GC = cov([truncU_GC G]);
Loadings_E_cov= cov([truncU_E E]); 

truncU_E_zscores= zscore(truncU_E);
truncU_GC_zscores= zscore(truncU_GC);
Predictor_weights_cov_GC_z = cov([truncU_GC_zscores G]);
Loadings_E_cov_z = cov([truncU_E_zscores E]);

fig7=figure;
plot(Predictor_weights_cov_GC(3:end, 1), Predictor_weights_cov_GC(3:end, 2), '.');
labelsG = {'MDD','MDM','SimpSchizophrenia','ParaSchizophrenia'};
text(Predictor_weights_cov_GC(3:end, 1), Predictor_weights_cov_GC(3:end, 2), labelsG);
xlabel =('Predictor1');
ylabel =('Predictor2');
title( 'Figure 1d - Covariance');

fig8=figure;
plot(Loadings_E_cov_z(3:end, 1), Loadings_E_cov_z(3:end, 2), '.');
text(Loadings_E_cov_z(3:end, 1), Loadings_E_cov_z(3:end, 2), symptomlabels)
title( 'Figure 1c - Covariance');


fig9=figure;
plot(Predictor_weights_cov_GC_z(3:end, 1), Predictor_weights_cov_GC_z(3:end, 2), '.');
labelsG = {'MDD','MDM','SimpSchizophrenia','ParaSchizophrenia'};
text(Predictor_weights_cov_GC_z(3:end, 1), Predictor_weights_cov_GC_z(3:end, 2), labelsG);
xlabel =('Predictor1');
ylabel =('Predictor2');
title( 'Figure 1d - Covariance_zscores');


fig10=figure;
plot(Loadings_E_cov(3:end, 1), Loadings_E_cov(3:end, 2), '.');
text(Loadings_E_cov(3:end, 1), Loadings_E_cov(3:end, 2), symptomlabels)
title( 'Figure 1c - Covariance_zscores');