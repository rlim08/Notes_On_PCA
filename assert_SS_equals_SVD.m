
function assert_SS_equals_SVD(Z, condition)
meanZ = mean(mean(Z));           % mean of Z
[m, n] = size(Z);
Sq_Z = (Z-meanZ).^2;             % Calculate the Squares of Z
SSq_Z=zeros(1,n);               % make new variable for the sum of columns
for i = 1:n                     % loop to compute sum of squares for each column
    SSq_Z(1,i) = sum(Sq_Z(:,i));
end
SSq_Z = sum(SSq_Z)/(m-1);          % Sum the Sum of Squares for all columns
                             % To get Variance divide by m or m-1
[U,D,V]=svd(Z, 'econ');
try 
    assert(abs(SSq_Z-(trace(D.^2))/(m-1))<0.001);
    disp(['Variance and eigenvalues are both equal on condition: ' condition ' and are ~' num2str(SSq_Z)]);
catch
    warning(['SVD fails on condition: ' condition]);
end
end